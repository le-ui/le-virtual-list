# le-virtual-list

### 使用方式

```javascript
<le-virtual-list 
	:list="list" 
    :virtua-height="800" 
    :item-height="100"
	:buffer-size="20">
        <!-- row：list的子数据 -->
        <template v-slot:item="{row}">
            <view class="content-item">
                  当前值：{{row}}
			</view>
		</template>
</le-virtual-list>
```

| 属性          | 类型   | 默认值 | 必填 | 说明                           |
| ------------- | ------ | ------ | ---- | ------------------------------ |
| list          | Array  | []     | 否   | 列表数组数据                   |
| virtua-height | Number | 700    | 否   | 虚拟列表的高度                 |
| item-height   | Number | 100    | 否   | 列表子组件高度                 |
| buffer-size   | Number | 20     | 否   | 显示区域的缓冲数量，前+n，后+n |
| customStyle   | Object | {}     | 否   | 自定义外层样式                 |

注意：微信小程序需要使用2.17.0版本及以下，否则会有slot重复问题。

